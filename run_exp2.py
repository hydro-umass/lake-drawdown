import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os
import scipy.signal
import datetime
from multiprocessing import Process

# Please make sure the working directory of the notebook is at the main folder
# If not, please us chdir function to change working directory
# os.chdir('..')
print(os.getcwd())

from models.RRMPG.rrmpg.models import CemaneigeGR4J
from models.WDLake.WD_Lake import Lake
from tools.metrics import calc_nse, calc_kge
from tools.flowstats import flow_duration

lake_info = pd.read_csv("data/WD_Lakes.txt", sep = ",", dtype = {'Use_Gauge':str})
lake_info = lake_info.loc[lake_info['Lake'] != "Silver"]
lake_info = lake_info.loc[lake_info['Lake'] != "Wyman"]
lake_info = lake_info.loc[lake_info['Lake'] != "Cranberry Meadow"]
# lake_info.head()

# Create an empty dict to save lake objects
lakes_list = [Lake(lname = lkname, 
           sim_date = pd.date_range('2010-01-01', '2020-12-30'), 
           val_date = pd.date_range('2015-10-31', '2018-06-01'), 
           wd_sim_date = pd.date_range('2015-11-01', '2018-08-01')) for lkname in lake_info['Lake'].values.ravel()]
lnames = list(lake_info['Lake'].values.ravel())
lakes = dict(zip(lnames, lakes_list))


lake_df = lake_info.set_index("Lake") # Find info by name
# Load CGR4J parameter
params_pool = pd.read_csv('data/HydroSimilarStations/parameters/Param_Metrics_RRMPG_CGR4J.txt',delimiter=',',index_col=0, dtype = {'Gauges':str})
#params_pool.head()

def run_dif_refill_depth(lname):
    # for convinient, use lak to indicate the lake object
    lk = lakes[lname]
    print(lname)
    # ---------------Load Lake Informations -----------------
    lk.contributing_area = lake_df.loc[lname,'Drainage Area_km2'] * 1e6 # km^2 to m2
    lk.height = lake_df.loc[lname,'Elevation_m'] # meter
    lk.lat = lake_df.loc[lname,'Lat'] # Latitude
    bath_path = lake_df.loc[lname,'bath_path']
    weather_type = 'daymet'
    daymet_weather_path = lake_df.loc[lname,'daymet_weather_path']
    daymet_basin_weather_path = daymet_weather_path.replace(lname, lname + "_basin_mean")
    # pwl = 20 #  Surface water - Deepest bottom, for reference
    obs_rwl_path =  lake_df.loc[lname,'obs_rwl_path']
    obs_rwl = pd.read_csv(obs_rwl_path, index_col=0, parse_dates=True) # meter
    lk.val_date = obs_rwl.index
    # lk.wd_sim_date = obs_rwl.index
    lk.load_bathymetry(bath_path=bath_path, nSteps=200, unit = 'ft', plot_fit_curve = False) # Required
    pwl = lk.ewl - obs_rwl.max()[0]# Max water level will be regarded as the top level

    #pwl = 0.9 * lk.ewl 
    lk.load_pvol_dvol(pwl=pwl) # Required, load the reference water level for calculating relative water levels
    lk.load_observations(obs_rwl = obs_rwl, obs_outflow = [], obs_area = []) # Optional if you don't have any
    lk.load_weather(weather_type=weather_type, csv_path=daymet_weather_path) # Required, here we are using daymet
    lk.load_basin_weather(weather_type=weather_type, csv_path=daymet_basin_weather_path)

    #----------------- Connect CGR4J -------------------
    gauge_id = lake_df.loc[lname,'Use_Gauge']
    params = params_pool.loc[gauge_id, ['CTG', 'Kf', 'x1', 'x2', 'x3', 'x4']]
    params = params.to_dict() # The model receieve the parameters as dict
    model = CemaneigeGR4J()
    model.set_params(params)
    inflow_type = 'cgr4j'
    lk.connect_cgr4j(model=model, model_unit = 'cfsm', stochastic=False)
    # no need to do stochastic inflow simulation again, already saved
    lk.cgr4j_inflows = np.load("data/Inflow_Simulations/Stochastic/{lname}_inflows.npy".format(lname = lname))

    # I have calibrated and saved the parameter
    param_path = 'data/Lake_Release_Parameters/MA/{lname}_calibrated.csv'.format(lname = lname)
    param_df = pd.read_csv(param_path, index_col=0)

    # Change to WD timings
    for i, refill in enumerate(range(0,90)):
    #for j, drawdown in enumerate(range(0,31)):
        for k, depth in enumerate(range(1,7)):
            param_df.loc["wd_level"] = - depth * 0.3048 * 1.05 # ft to meter
            param_df.loc["T_startrefill"] = 366 + refill
            param_df.loc["T_stopdrawdown"] = 335
            param_df.loc["T_startdrawdown"] = 305
            param_df.loc["gamma_max"] = 91 - refill
            param_df.loc["alpha_max"] = 30
            # lk.wd_sim_date = pd.date_range(pd.Timestamp(datetime.datetime.strptime(param_df.columns[0] + "-" + str(int(param_df.iloc[2,0])), "%Y-%j")), pd.Timestamp(datetime.datetime.strptime( str (int(param_df.columns[-1]) + 1) + "-" + str(int(param_df.iloc[5,-1]) - 365), "%Y-%j")))
            initial_wl = lk.pwl
            lk.Simulate_WD_stochastic(param_df,
                                    initial_wl,
                                    weather_uncertainty = True,
                                    iteration_times = 1000,
                                    Qlim_unit = "m3s",
                                    a_or_ta = "a")
            # Save all the scenario results
            np.savez_compressed("/home/xinchenh/work/data/MA_Sim_Scenarios/Exp2/{lname}_sim_rwl_refill_{refill}_dh_{depth}ft".format(lname = lname, refill = refill, depth = depth), 
                                dates = lk.wd_sim_date, wl = lk.sim_rwl_stochastic, outflow = lk.sim_outflow_stochastic, inflow = lk.sim_inflow_stochastic)
            
    print("Lake {lname} done".format(lname = lname))
    
    

# RUN
ps = []
#create jobs
for i in range(15):
    ps.append(Process(target = run_dif_refill_depth, args = [lnames[i]]))

#excute jobs at same time
for i in range(15):
    ps[i].start()