"""
_A script to access Streamstats to delineate watersheds and save basin characteristics_
    @ Author: Xinchen He
    @ Date: 10/13/2022
"""
import pandas as pd
import streamstats
import geopandas as gpd

# Points coordinates: [Lat, Lon]
lake_outlets = pd.read_csv('data/Streamstats_Data/lake_outlets_unfinished.txt', sep = '\t', index_col=0)
# Save dirs of delineate watershed shape files and characteristics
basin_shp_save_dir = 'data/Streamstats_Data/basin_shps/'
basin_characteristics_save_dir = 'data/Streamstats_Data/basin_chars/'

# Loop each point
for lake in lake_outlets.index:
    # Sometimes streamstats does not respond
    try:
        lat = lake_outlets.loc[lake, 'Lat']
        lon = lake_outlets.loc[lake, 'Lon']
        ws = streamstats.Watershed(lat=lat, lon=lon)

        # Save watershed to shp files
        poly = gpd.GeoDataFrame.from_features(ws.boundary["features"], crs="EPSG:4326")
        poly.to_file(basin_shp_save_dir + lake + '.shp', driver = 'ESRI Shapefile')
        print(lake + '.shp', ' successfully saved')

        # Save the basin characteristics to csv file
        codes = []
        descriptions = []
        values = []
        units = []
        for char in list(ws.characteristics.keys()):
            codes.append(ws.get_characteristic(char)['code'])
            descriptions.append(ws.get_characteristic(char)['description'])
            values.append(ws.get_characteristic(char)['value'])
            units.append(ws.get_characteristic(char)['unit'])
        df = pd.DataFrame(zip(codes, descriptions, values, units), columns=['Parameter Code','Parameter Description','Value','Unit'])
        df.to_csv(basin_characteristics_save_dir + lake + '_streamstats.csv', index = False)
        print(lake + ' characteristics successfully saved')
    except:
        # Get info from streamstats web application
        print('Failed to get info of ' + lake)
        continue