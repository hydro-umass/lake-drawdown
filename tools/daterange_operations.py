'''
This script is for saving tools handling with pandas Time stamp and date range
'''

import pandas as pd


def common_period(period_1, period_2):
    # Find the period which is both in period_1 and period_2

    start_time = max(period_1[0], period_2[0])
    end_time = min(period_1[-1], period_2[-1])

    if start_time < end_time:
        new_period = pd.date_range(start_time, end_time)
        return new_period
    else:
        return [] # no common period

    
