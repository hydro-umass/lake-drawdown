from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import numpy as np
import spotpy
import os
from distutils.dir_util import copy_tree, remove_tree
#from shutil import rmtree
import sys
import pandas as pd
from models.swatplus.swatplus_python import swatplus
import matplotlib.pyplot as plt

'''
Calibration SWAT+ model by using Spotpy
'''

class spot_setup(object):

    def __init__(self, model_dir, exe_path, obs_dir, channel_ID, res_ID = '', parallel = 'seq'):
        #Find Path to swatplus on users system
        self.owd = os.path.dirname(os.path.realpath(__file__))
        self.model_dir = model_dir
        self.exe_path = exe_path
        self.obs_dir = obs_dir
        self.channel_ID = channel_ID
        self.res_ID = res_ID

        # Create uniform distributed parameters
        self.params = [spotpy.parameter.Uniform('cn2', low=35 , high=95),
                        spotpy.parameter.Uniform('awc', low=0.01 , high=1),
#                        spotpy.parameter.Uniform('k', low=0.0, high=2000.0),
                        spotpy.parameter.Uniform('esco', low=0 , high=1),
                        spotpy.parameter.Uniform('epco', low=0 , high=1),
#                        spotpy.parameter.Uniform('surlag', low=0.05 , high=24),
                        spotpy.parameter.Uniform('perco', low=0 , high=1.0),
#                        spotpy.parameter.Uniform('lat_ttime', low=0.5, high=180),
#                        spotpy.parameter.Uniform('alpha', low=0 , high=50),
#                        spotpy.parameter.Uniform('revap_co', low=0.02 , high=0.2),
#                        spotpy.parameter.Uniform('flo_min', low=0 , high=50),
#                        spotpy.parameter.Uniform('canmax', low=0 , high=100),
                        spotpy.parameter.Uniform('revap_min', low=0 , high=50)
        ]

        # Specify whether or which parallel you would use
        self.parallel = parallel
                    

        # Find out sim_date
        # Read the time.sim and print.prt to find out the simulation date
        with open(os.path.join(model_dir, 'time.sim'), 'r') as f:
            line = f.readlines()[2].split() # Day start, year start, day end, year end, step
        
        with open(os.path.join(model_dir, 'print.prt'), 'r') as f2:
            warm_up_years = int(f2.readlines()[2].split()[0])

        start_date_sim = pd.to_datetime(int(line[0]), unit = 'D', origin = str(int(line[1]) + warm_up_years))
        end_date_sim = pd.to_datetime(int(line[2]) - 1, unit = 'D', origin = str(line[3]))

        # Load Observed data
        if self.res_ID == '':# This means calibrating a channel
            obs = pd.read_csv(os.path.join(self.obs_dir, self.channel_ID + '.csv'), index_col=0, parse_dates=True)
        else:
            obs = pd.read_csv(os.path.join(self.obs_dir, self.res_ID + '.csv'), index_col=0, parse_dates=True)
            
        start_date_obs = obs.index[0]
        end_date_obs = obs.index[-1]

        self.date = pd.date_range(max(start_date_obs, start_date_sim), min(end_date_obs, end_date_sim))

        # Save the simulated/observed channel flow/water level
        self.trueObs = obs.loc[self.date].values.ravel()

        print('The shape of observed flow is',self.date)

        

    def parameters(self):
        return spotpy.parameter.generate(self.params)

    def simulation(self, x):
        # print(x)
        # Select if it's parrallel computing
        if self.parallel == 'seq':
            call = ''
        elif self.parallel == 'mpi':
            #Running n parallel, care has to be taken when files are read or written
            #Therefor we check the ID of the current computer core 
            call = str(int(os.environ['OMPI_COMM_WORLD_RANK'])+2)
            #And generate a new folder with all underlying files
            copy_tree(self.model_dir, self.model_dir+call)
        elif self.parallel == 'mpc':
            #Running n parallel, care has to be taken when files are read or written
            #Therefor we check the ID of the current computer core 
            call =str(os.getpid())
            #And generate a new folder with all underlying files
            copy_tree(self.hymod_path, self.hymod_path+call)        
        else:
            raise 'No call variable was assigned'


        model = swatplus(self.model_dir+call, self.exe_path) # Change the original model dir to new created folder
        model.write_params(x)
        model.run(compress = False, wine = True)

        if self.res_ID == '':# This means calibrating a channel
            sim = model.read_channel(channelID=self.channel_ID, channel_cols=['flo_out']).loc[self.date].values.ravel()
        else:
            sim = model.read_reservoir(res_id=self.res_ID, res_cols=['flo_in']).loc[self.date].values.ravel()

        if self.parallel == 'mpi' or self.parallel == 'mpc':
            remove_tree(self.model_dir+call)
        return sim

    def evaluation(self):
        return self.trueObs

    def objectivefunction(self,simulation,evaluation, params=None):
        like = -spotpy.objectivefunctions.nashsutcliffe(evaluation,simulation)     # Because the algorithm finds the minimum objective function, I put a `-` before
        return like



def main():
    #!/usr/bin/env python
    ## Windows
    model_dir = 'data/swatplus_results/gb1.0_cal/swatplus_model'
    exe_path = 'models/swatplus/swatplus_exes/swatplus_official_rel_600504.exe'
    obs_dir = 'data/swatplus_results/gb1.0_cal/observations'
    parrallel_type = 'seq' # 'seq', 'mpi', 'mpc'
    channel_ID = '001'

    # Select number of maximum repetitions
    rep = 200

    # Set up the model
    spot_setup = spot_setup(model_dir, exe_path, obs_dir, channel_ID, parallel = parrallel_type)

    # Now we create a sampler, 
    # by using one of the implemented algorithms in SPOTPY. 
    # The algorithm needs the inititalized spotpy setup class, 
    # and wants you to define a database name and database format
    sampler = spotpy.algorithms.sceua(spot_setup, dbname = 'SCEUA_swatplus', dbformat = 'csv')

    # Settings are from https://spotpy.readthedocs.io/en/latest/Calibration_with_SCE-UA/
    sampler.sample(rep, ngs=7, kstop=3, peps=0.1, pcento=0.1)

    # Load results
    print('starting to load results')
    results = spotpy.analyser.load_csv_results('SCEUA_swatplus')
    print('results loaded successfully')

    fig= plt.figure(1,figsize=(9,5))
    plt.plot(results['like1'])
    #plt.show()
    plt.ylabel('NSE')
    plt.xlabel('Iteration')
    fig.savefig('data/swatplus_results/SCEUA_objectivefunctiontrace.png',dpi=300)


main()