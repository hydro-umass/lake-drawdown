import rasterio
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.optimize import curve_fit
from scipy import interpolate

def cal_Vol(bath_path, nSteps):
    """_summary_

    Args:
        bath_path (_str_): _The path to the TIFF file_
        nSteps (_int_): _Specify how many steps for the calculation_

    Returns:
        volArray (_ndarray_): _The array for different volume in different elevations_
        areaArray (_ndarray_): _The array for different area in different elevations_
        elevSteps (_ndarray_): _The elevation array, related to nSteps_
    """
    lakeRst = rasterio.open(bath_path)
    lakeBottom = lakeRst.read(1) * 0.3048 # ft to m
    # replace value for np.nan
    noDataValue = np.copy(lakeBottom[0,0])
    lakeBottom[lakeBottom==noDataValue]= np.nan
    # get raster minimum and maximum 
    minElev = np.nanmin(lakeBottom)
    maxElev = np.nanmax(lakeBottom)
    # lake bottom elevation intervals
    elevSteps = np.round(np.linspace(minElev,maxElev,nSteps),2)

    # calculate volumes for each elevation
    volArray = []
    areaArray = []

    # definition of volume function
    def calculateVol(elevStep,elevDem,lakeRst):
        tempDem = elevDem[elevDem<=elevStep]
        tempVol = tempDem.sum()*lakeRst.res[0]*lakeRst.res[1]
        temp_area = len(tempDem) * lakeRst.res[0]*lakeRst.res[1]
        return tempVol, temp_area

    for elev in elevSteps:
        tempVol, temp_area = calculateVol(elev,lakeBottom,lakeRst)
        volArray.append(tempVol)
        areaArray.append(temp_area)
    
    return volArray, areaArray, lakeBottom, elevSteps

def plot_bath(lakeBottom, lakename):
    fig, ax1 = plt.subplots(1,1,figsize=(8,6))
    ax1.set_title('Bathymetry')
    botElev = ax1.imshow(lakeBottom)
    fig.colorbar(botElev, orientation='horizontal', label='WaterDepth m')
    plt.suptitle(lakename)
    plt.show()

def plot_fit_vol_area(lakeBottom, volArray, areaArray, lakename, savefig = False, fig_dir = ''):
    """_A function to plot the curve of volume and area_

    Args:
        lakeBottom (_2D array_): _The ndarray of the original bathymetry file_
        volArray (_ndarray_): _The array for different volume in different elevations_
        areaArray (_ndarray_): _The array for different area in different elevations_
        lakename (_str_): _Name of this Lake_
        savefig (_boolean_):_See if you want to save the figures as png. e.g.Onota_vol_area.png_
    """
    # plot values
    fig, [ax1, ax2] = plt.subplots(1,2,figsize=(12,4))
    ax1.set_title('Bathymetry')
    botElev = ax1.imshow(lakeBottom)

    divider = make_axes_locatable(ax1)
    cax = divider.append_axes('bottom', size='5%', pad=0.5)
    fig.colorbar(botElev, cax=cax, orientation='horizontal', label='WaterDepth m') 

    # Fit with br1 * V ** br2
    def func(x, a, b):
        return a * x **b
    x = volArray
    y = areaArray
    br1, br2 = curve_fit(func,x,y)[0]

    ax2.scatter(volArray,areaArray,label = 'Bathymetry')
    ax2.plot(x, func(x, br1, br2), color = 'r', label="Fitted Curve, {br1}V^{br2}".format(br1 = np.round(br1,3), br2 = np.round(br2,3)))
    ax2.grid()
    ax2.legend()
    ax2.set_xlabel('Volume m3')
    ax2.set_ylabel('area m2')
    plt.suptitle(lakename)
    plt.show()
    if savefig == True:
        plt.savefig(fig_dir + '/'+ lakename+'_vol_area.png', dpi = 300)

def get_evol_pvol_dvol(volArray, areaArray, elevSteps, parea, earea = None, reduce_wl = 3 * 0.3048, degree = 10):
    """_summary_

    Args:
        volArray (_ndarray_): _The array for different volume in different elevations_
        areaArray (_ndarray_): _The array for different area in different elevations_
        elevSteps (_ndarray_): _The elevation of the surface water level_
        parea (_ndarray_): _The principal spillway area of parea_
        earea (_type_, optional): _description_. Defaults to None.
        dh (_type_, optional): _description_. Defaults to 3*0.3048.

    Returns:
        _type_: _description_
    """
    # Fit with br1 * V ** br2
    def func(x, a, b):
        return a * x **b
    x = volArray
    y = areaArray
    br1, br2 = curve_fit(func,x,y)[0] # get the coefficients for vol_area

    pvol = (parea/br1)**(1/br2)# principle spillway volume
    if earea == None:
        earea = areaArray[-1] # if there is no value for earea, the default will set it as the area of the bathymetry
        evol = volArray[-1] # emergency spillway volume

    # fit the curve of the vol and h. Use 5 degree polynomial regression.Need to pass through (0,0)   
    #f1 = np.poly1d(np.polyfit(elevSteps,volArray,degree))
    #f2 = np.poly1d(np.polyfit(volArray,elevSteps,degree))

    f1 = interpolate.InterpolatedUnivariateSpline(elevSteps, volArray)
    f2 = interpolate.InterpolatedUnivariateSpline(volArray, elevSteps)
    
    # Get water levels of each water level
    p_wl = f2(pvol) # principal spillway water level
    e_wl = f2(evol) # emergency spillway water level
    d_wl = p_wl - reduce_wl # winter drawdown water level, default is 3ft drawdown
    dvol = f1(d_wl) # winter drawdown remained volume
    darea = br1 * dvol ** br2
    # perc = dvol/pvol # percentage of remained volume to pvol after drawdown

    return (evol,earea,e_wl), (pvol, parea, p_wl), (dvol, darea, d_wl), (br1,br2), (f1,f2)
