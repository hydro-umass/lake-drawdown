import numpy as np
import pandas as pd
from models.pyet.pyet.radiation import oudin


def preprocess_weather(weather_df, weather_type, lat = 42.4072):
    """_It's a tool for processing the raw input weather pandas dataframe_

    Args:
        weather_df (_Pandas Dataframe_): _raw pandas dataframe import from other_
        weather_type (_str_): _daymet or gridmet_
        lat (float, optional): _Latitude of observed area_. Defaults to 42.4072, which is Boston, MA

    Returns:
        _Pandas DataFrame_: _A dafaframe for building and training a CGR4J model_
    """

    if weather_type == 'gridmet':
        df = weather_df.copy()
        #print(df.columns)
        df['tmax'] = df['tmmx'] - 273.15 # C
        df['tmin']= df['tmmn'] - 273.15 # C
        df['tmean'] = (df['tmax'] + df['tmin'])/2 # C
        df['rs'] = df['srad'] * 86400/1000000 # mj/m2/day
        # df['pet'] = df['eto'] # reference evaporation for grass
        df['pet'] = oudin(tmean = df['tmean'], lat = lat * np.pi /180)
        df['prec'] = df['pr']
        df['month'] = df.index.month
        df = df.loc[:,['tmin', 'tmax', 'tmean', 'prec', 'pet', 'month']]

    elif weather_type == 'daymet':
        df = weather_df.copy()
        df['tmean'] = (df['tmax'] + df['tmin'])/2 # C
        df['prec'] = df['prcp']
        df['rs'] = df['srad'] * 86400/1000000 # mj/m2/day
        df['pet'] = oudin(tmean = df['tmean'], lat = lat * np.pi /180)
        df['month'] = df.index.month
        df = df.loc[:,['tmin', 'tmax', 'tmean', 'prec', 'pet', 'month']]
        
    return df
    
