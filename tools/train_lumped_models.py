"""
This script is to train a CemaneigeGR4J model and return parameters after calibration

"""
import numpy as np
import pandas as pd
from tools.weather import preprocess_weather
from models.RRMPG.rrmpg.models import CemaneigeGR4J, HBVEdu


def build_train_CemaneigeGR4J(weather_type, obs_flow, weather_df, gauge_height, lat = None):
    """_A function to build, calibrate a CGR4J model_

    Args:
        weather_type (_str_): _'gridmet', 'daymet', 'prism', or 'observation'_
        weather_df (_pd.DataFrame_): _A pandas dataframe which should contain columns of [tmin, tmax, tmean, pr, etr]_
        obs_flow (_pd.DataFrame_): _A pandas dataframe of outflow observations_
        gauge_height (_float_): _The elevation of the outflow gauge_

    Returns:
        _model_: _A calibrated CG4J model_
    """
    # initializa a gr4j model
    model = CemaneigeGR4J()

    # import the weather data
    if weather_type == 'observation':
        df = weather_df
    else:
        df = preprocess_weather(weather_df=weather_df, weather_type=weather_type, lat = lat)
    
    # the simulation period has set as the flow observations
    sim_date = obs_flow.index

    # create sim dataframe
    sim = df.loc[sim_date]
    
    sim['flow'] = obs_flow.values.ravel()

    # use the default fit function to calibrate nse
    result = model.fit(sim['flow'], sim['prec'], sim['tmean'], sim['tmin'], sim['tmax'], sim['pet'], gauge_height)

    print(result)

    # get the parameters
    params = {}

    param_names = model.get_parameter_names()

    for i, param in enumerate(param_names):
        params[param] = result.x[i]

    # This line set the model parameters to the ones specified in the dict
    model.set_params(params)

    # To be sure, let's look at the current model parameters
    # model.get_params()

    return model

def build_train_HBV(weather_type, obs_flow, weather_df, gauge_height, lat = None):
    """_A function to build, calibrate a CGR4J model_

    Args:
        weather_type (_str_): _'gridmet', 'daymet', 'prism', or 'observation'_
        weather_df (_pd.DataFrame_): _A pandas dataframe which should contain columns of [tmin, tmax, tmean, pr, etr]_
        obs_flow (_pd.DataFrame_): _A pandas dataframe of outflow observations_
        gauge_height (_float_): _The elevation of the outflow gauge_

    Returns:
        _model_: _A calibrated CG4J model_
    """
    # initializa a hbv model
    model = HBVEdu()

    # import the weather data
    if weather_type == 'observation':
        df = weather_df
    else:
        df = preprocess_weather(weather_df=weather_df, weather_type=weather_type, lat = lat)

    # From the weather data, we calculate long term tmean and pet in each month
    PE_m = df.groupby("month").mean()['pet'].values.ravel()
    T_m = df.groupby("month").mean()['tmean'].values.ravel()
    
    # the simulation period has set as the flow observations
    sim_date = obs_flow.index

    # create sim dataframe
    sim = df.loc[sim_date]
    
    sim['flow'] = obs_flow.values.ravel()

    # use the default fit function to calibrate nse
    result = model.fit(sim['flow'], sim['tmean'],
                       sim['prec'], sim['month'],
                       PE_m, T_m)
    
    print(result)

    # get the parameters
    params = {}

    param_names = model.get_parameter_names()

    for i, param in enumerate(param_names):
        params[param] = result.x[i]

    # This line set the model parameters to the ones specified in the dict
    model.set_params(params)

    # To be sure, let's look at the current model parameters
    # model.get_params()

    return model

def build_train_MILC(obs_flow, weather_df, contributing_area, lat):
    # This is based on Lumod
    # Not working well
    
    # Initial requirements
    nsims = 5000  # number of simulations
    keepb = 20    # keep only the best simulations
    savevars = ["qt"]  # save only this variables from Monte Carlo simulation

    xobs = pd.DataFrame(obs_flow.values.ravel(), columns=['qt'], index=obs_flow.index)
    forcings = weather_df

    # Create a model
    model = lumod.models.MILC(area=contributing_area, lat=lat)

    # Bounds of Parameters
    bounds = {
        "wmax": (1000,5000),
        "gamma": (10.0, 25.0),
        'm': (1, 20),
        'ks': (100, 5000),
        'nu': (0.01, 0.4),
        "alpha":(0.1, 15.0)
    }


    # Objective function
    score1 = {
        "var": "qt",
        "metric": "kge",
        "weight": 1.0
    }

    scores = [score1]

    # Run Monte Carlo

    sim_date = obs_flow.index

    mc_res = lumod.MonteCarlo(
        model,
        forcings,
        bounds,
        numsimul=nsims,
        save_vars=savevars,
        xobs=xobs,
        scores=scores,
        keep_best=keepb,
        start=sim_date[0].strftime('%Y-%m-%d'),
        end=sim_date[-1].strftime('%Y-%m-%d')
    )

    # save parameters
    fitted_parameters = mc_res["parameters"].iloc[0]
    model.set_parameters(fitted_parameters)

    return model