import rasterio
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.optimize import curve_fit
from scipy import interpolate


def read_bathymetry(bath_path, nSteps = 100, unit = 'meter'):
    """_Read the bathymetry file to get v_h, h_v, v_a relationship_

    Args:
        bath_path (_type_): _description_
        parea (_float_): _The lake area of the principle spillway volume_
        nSteps (int, optional): _Specify how many layers we would like to separate bathymetry files_. Defaults to 100.
        unit (str, optional): _Specify ft or meter used in the bathymetry file_. Defaults to 'meter'.
    """
    print('============Reading Bathymetry=============')

    lakeRst = rasterio.open(bath_path)
    
    # unit
    if unit == 'ft':
        lakeBottom = lakeRst.read(1) * 0.3048 # ft to m
    else: # unit == 'meter' 
        lakeBottom = lakeRst.read(1)

    # replace value for np.nan
    noDataValue = np.copy(lakeBottom[0,0])
    lakeBottom[lakeBottom==noDataValue]= np.nan
    # get raster minimum and maximum 
    minElev = np.nanmin(lakeBottom)
    maxElev = np.nanmax(lakeBottom)
    # lake bottom elevation intervals
    elevSteps = np.round(np.linspace(minElev,maxElev,nSteps),2)

    # calculate volumes for each elevation
    volArray = []
    areaArray = []

    # define of volume function
    def calculateVol(elevStep,elevDem,lakeRst):
        tempDem = elevDem[elevDem<=elevStep]
        tempVol = tempDem.sum()*lakeRst.res[0]*lakeRst.res[1]
        temp_area = len(tempDem) * lakeRst.res[0]*lakeRst.res[1]
        return tempVol, temp_area

    for elev in elevSteps:
        tempVol, temp_area = calculateVol(elev,lakeBottom,lakeRst)
        volArray.append(tempVol)
        areaArray.append(temp_area)

    # Fit with br1 * V ** br2
    def func(x, a, b):
        return a * x **b
    x = volArray
    y = areaArray
    br1, br2 = curve_fit(func,x,y)[0] # get the coefficients for vol_area
    v_a = (br1, br2) # get v-a relationship
    print('v_a loaded')

    # fit the curve of the vol and h. Use 5 degree polynomial regression.Need to pass through (0,0)   
    # f1 = np.poly1d(np.polyfit(elevSteps,volArray,degree))
    # f2 = np.poly1d(np.polyfit(volArray,elevSteps,degree))

    f1 = interpolate.InterpolatedUnivariateSpline(elevSteps, volArray)
    f2 = interpolate.InterpolatedUnivariateSpline(volArray, elevSteps)
    h_v = f1
    print('h_v loaded')
    v_h = f2
    print('v_h loaded')
    
    return


if __name__ == "__main__":
    bath_path = 'data/HS_Bathymetries/richmond_pond_1m_bathymetry1.tif'
    read_bathymetry(bath_path=bath_path, nSteps=100, unit = 'ft')