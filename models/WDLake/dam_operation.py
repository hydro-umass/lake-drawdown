import numpy as np
import matplotlib.pyplot as plt

def generate_dam_operation(initial_height, target_height, required_days, daily_constraint, num_schemes, max_iteration = 50000):
    """_A tool to generate dam operation schemes to lower the gate to target height in required days with daily restrictions_

    Args:
        initial_height (_float_): _initial dam height_
        target_height (_type_): _target drawdown depth, which is the target gate level_
        required_days (_type_): _description_
        daily_constraint (_type_): _description_
        num_schemes (_type_): _description_
        max_iteration (int, optional): _description_. Defaults to 50000.

    Returns:
        _type_: _description_
    """
    
    # for example, require_days = 30.
    
    # sum of the dam differences. for example, 1m drawdown, summ_diffs = 1m
    sum_diffs = initial_height - target_height
    
    # empty list to save dam heights
    dam_heights = []
    # dam height change per day should be in the range of [a,b]
    a, b = daily_constraint
    
    for j in range(max_iteration):
        
        dam_height_each = np.zeros(required_days)
        dam_height_each[0] = initial_height
        
        height_diff_each = (b-a) * np.random.random_sample(size = required_days-1) + a
        # clear the last value to calculate first 28 days
        height_diff_each[-1] = 0
        
        
        # filter samples when the sum diffs can possibly be sum_diffs
        if (sum_diffs - b) <= sum(height_diff_each) <= sum_diffs:
            # calculate the last value
            height_diff_each[-1] = sum_diffs - sum(height_diff_each)
            # use the height diff to calculate the dam height
            for i in range(1, required_days):
                dam_height_each[i] = dam_height_each[i-1] - height_diff_each[i-1]
            # save the dam heights
            dam_heights.append(dam_height_each)
            
        if len(dam_heights) == num_schemes:
            print(num_schemes, "gate operation schemes have generated")
            break
    
    if len(dam_heights) < num_schemes:
        print("Unable to generate", num_schemes, "schemes, please increase max_iteration number")
        return
        
    
    # Define a quick release, active dam operation scheme
    # First n days use 3inch/day, rest days would be 0
    early_scheme = np.zeros(required_days)
    early_scheme[0] = initial_height
    # first n days as max lowering. For example, first 13 days need to 3inch/d
    n = int((initial_height - target_height)//b)
    for i in range(1, int(n) + 1):
        early_scheme[i] = early_scheme[i-1] - b
    # day 14 need to lower 0.009399999999999964, rest no change
    early_scheme[(int(n) + 1):] = early_scheme[n] - (initial_height - target_height)%b
    
    # Define a slow release, late dam operation
    late_scheme = np.zeros(required_days)
    # only need to operate n+1 day, full release n days, rest of days, no need to operate
    n = int((initial_height - target_height)//b)
    rest_n = required_days - n - 1
    late_scheme[:rest_n] = initial_height
    late_scheme[rest_n] = late_scheme[rest_n - 1] - (initial_height - target_height)%b
    for i in range(1,n+1):
        late_scheme[rest_n+i] = late_scheme[rest_n + i - 1] - b
        
    return dam_heights, early_scheme, late_scheme

if __name__ == "__main__":
    
    initial_height = 15 #m
    target_height = 15 - 1 #m, 1m drawdown
    required_days = 30 # lower the gate in 30 days
    daily_constraint = [0, 0.0762] # 3inch/d to m/d
    num_schemes = 10000
    
    dam_schemes, early_scheme, late_scheme = generate_dam_operation(initial_height, target_height, required_days, daily_constraint, num_schemes, max_iteration = 100000)
    
    for i in range(len(dam_schemes)):
        plt.plot(np.arange(required_days) + 1, dam_schemes[i])
    
    plt.plot(np.arange(required_days) + 1, early_scheme, label = "Early")
    plt.plot(np.arange(required_days) + 1, late_scheme, label = "Late")
    plt.title("Possible Dam operation schemes")
    plt.xlabel("Time")
    plt.ylabel("Dam Height/m")
    plt.legend()    
    plt.show()