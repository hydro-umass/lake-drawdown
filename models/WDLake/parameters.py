import numpy as np
import pandas as pd

def read_WD_parameter(df, year, h_v):

    ewl, pwl, dwl, Qmin_d, Qmax_d, Qmin_r, Qmax_r, Qmin_fc, Qmax_fc, alpha, beta, gamma, theta, t1, t2 = df[str(year)].values.ravel()

    operation_coeffs = (float(alpha), float(beta), float(gamma), float(theta))
    wd_timings = (int(t1), int(t2))
    Qlim =  np.array([Qmin_d, Qmax_d, Qmin_r, Qmax_r, Qmin_fc, Qmax_fc], dtype = float) * 86400

    evol = h_v(float(ewl))
    pvol = h_v(float(pwl))
    dvol = h_v(float(dwl))

    return wd_timings, operation_coeffs, Qlim, evol, pvol, dvol

def read_WD_periods(param):
    # This function is to read the parameter dataframe
    # And extract the simulation period from the parameter dataframe
    
    years = param.columns # years

    # Create sim_days_list and sim_dates_list to save the simulation period of each year in the form of dayofyear and pd.daterange
    sim_days_list = []
    sim_dates_list = []

    for i in range(len(years)):
        # The start of a WD year should be the first drawdown date recorded in the parameter file
        start_j = int(param.loc["T_startdrawdown", years[i]]) # drawdown start date of the first year
        # convert to time stamp
        start_time_stamp = pd.to_datetime(str(years[i]) + str(start_j), format = "%Y%j")

        # If it is last year, then set the stop day as the stop refilling day
        if i == len(years)-1:
            stop_j = int(param.loc["T_stoprefill",  years[i]])
            # convert the stop date to time stamp
            if int(years[i]) %4 == 0: # leap year
                stop_time_stamp = pd.to_datetime(str(int(years[i]) + 1) + str(stop_j - 366), format = "%Y%j") # refill is in next year
            else:
                stop_time_stamp = pd.to_datetime(str(int(years[i]) + 1) + str(stop_j - 365), format = "%Y%j") # refill is in next year
        else: # set the stop day as the start drawdown date of next year
            if int(years[i]) % 4 == 0: # leap year
                stop_j = int(param.loc["T_startdrawdown",  years[i+1]]) + 366
                # convert the stop date to time stamp
                stop_time_stamp = pd.to_datetime(str(int(years[i]) + 1) + str(stop_j - 366), format = "%Y%j") # refill is in next year
            else:
                stop_j = int(param.loc["T_startdrawdown",  years[i+1]]) + 365
                stop_time_stamp = pd.to_datetime(str(int(years[i]) + 1) + str(stop_j - 365), format = "%Y%j") # refill is in next year

        sim_days = np.arange(start_j, stop_j+1) # include the last day
        sim_date = pd.date_range(start_time_stamp, stop_time_stamp)# include the last day

        sim_dates_list.append(sim_date)
        sim_days_list.append(sim_days)
        total_sim_period = pd.date_range(sim_dates_list[0][0], sim_dates_list[-1][-1])

    return sim_days_list, sim_dates_list, total_sim_period
