import requests
import urllib
from streamstats import Watershed

def query_elevation(lat, lon):
    """Query service using lat, lon. add the elevation values as a new column."""

    # USGS Elevation Point Query Service
    url = r'https://nationalmap.gov/epqs/pqs.php?'
    # create data frame

    # define rest query params
    params = {
        'output': 'json',
        'x': lon,
        'y': lat,
        'units': 'Meters'
    }

    result = requests.get((url + urllib.parse.urlencode(params)))
    elevation = result.json()['USGS_Elevation_Point_Query_Service']['Elevation_Query']['Elevation']

    return elevation

def query_contributing_area(lat, lon): # Potential function to connect with streamstats to get drainage area, etc.
    """_This is a function to use streamstats api to delineate a watershed based on the coordinate of the lake outlet_

    Args:
        lat (_float_): _Latitude of the lake outlet_
        lon (_float_): _Lontitude of the lake outlet_
    """
    print("Querying Streamstats")
    
    # initial a watershed object
    ws = Watershed(lat = lat, lon = lon)

    print("Watershed information query complete")
    # load drainage_area
    contributing_area = ws.get_characteristic('DRNAREA')['value']  # SQUARE MILE

    return contributing_area